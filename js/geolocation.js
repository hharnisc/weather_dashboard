WU_API_KEY = 'XXXXXXXXXXXXXXXX'
function getGeolocation(){
	//attempt to get geolocation for browser 
	if(navigator.geolocation){
		navigator.geolocation.getCurrentPosition(alertPosition, geoLocErrorHandler);
	}
	//otherwise get geolocation from fallback source
	else{
		getFallBackGeolocation();
	}
}

//simple alert for testing 
function alertPosition(position){
	var lat = position.coords.latitude;
	var lon = position.coords.longitude;
	setPosition(lat,lon);
	
}	

//set position and begin grabing weather data
function setPosition(lat,lon){
	getWeatherLatLon(lat,lon);
}

//error reporting to user 
function geoLocErrorHandler(error){
	//get location from fallback source if
	//failed to get it from browser 
	getFallBackGeolocation();
}

//get location from weather underground
function getFallBackGeolocation(){
	//use autoip service with jsonp call to grab lat,lon
	var url = "http://api.wunderground.com/api/" + WU_API_KEY + "/geolookup/q/autoip.json?callback=?";
	$.getJSON(url, function(data){
		//get lat and lon from weather underground
		if(data.hasOwnProperty('location')){
         	setPosition(data['location']['lat'],data['location']['lon']);
		}
		//when all else fails return Boulder, CO
		else{
			setPosition(40.009594,-105.253912);
			
		}
	});
}

