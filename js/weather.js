WU_API_KEY = 'XXXXXXXXXXXXXXXX'

//get weather based on latitude and longitude
function getWeatherLatLon(lat,lon){
	var url = "http://api.wunderground.com/api/" + WU_API_KEY + "/conditions/forecast7day/q/" +lat + 
		"," + lon + ".json?callback=?";
		
	$.getJSON(url, function(data){
		//get lat and lon from weather underground
		console.debug(data);
		updateWeatherData(data);
	});
}

//get weather based on state and city
function getWeatherStateCity(state,city){
	var url = "http://api.wunderground.com/api/" + WU_API_KEY + "/conditions/forecast7day/q/"+ state +
		"/" + city +".json?callback=?";
	$.getJSON(url, function(data){
		//get lat and lon from weather underground
		console.debug(data);
		updateWeatherData(data);
	});
}

//update both current weather and forecast data
function updateWeatherData(data){
	//check if we have valid input for current weather 
	if(data.hasOwnProperty('current_observation')){
		populateCurrentWeather(data['current_observation']['display_location']['full'],
			data['current_observation']['weather'], 
			"http://icons.wxug.com/i/c/a/" +data['current_observation']['icon'] +".gif",
			data['current_observation']['temp_f'], data['current_observation']['windchill_f'],
			data['current_observation']['relative_humidity'], 
			data['current_observation']['visibility_mi']);
	}
	else{
		$('#cur_weather').html("Could Not Get Current Weather");
		displayMessage('Could Not Get Current Weather');
	}
	//check if we have good forecast data
	if(data.hasOwnProperty('forecast')){
		day_array = Array();
		//add each day's forcast information
		for(i=0; i < 7; i++){
			hi = data['forecast']['simpleforecast']['forecastday'][i]['high']['fahrenheit'];
			lo = data['forecast']['simpleforecast']['forecastday'][i]['low']['fahrenheit'];
			icon =  "http://icons.wxug.com/i/c/a/" + 
					data['forecast']['simpleforecast']['forecastday'][i]['icon'] + ".gif";
			cond = data['forecast']['simpleforecast']['forecastday'][i]['conditions'];
			day = data['forecast']['simpleforecast']['forecastday'][i]['date']['day'];
			wk_day = data['forecast']['simpleforecast']['forecastday'][i]['date']['weekday_short'];
			day_array.push(new Day(hi,lo,icon, cond, day, wk_day));
		}
		populateForcast(day_array,7);
		updateTempsChart(day_array);
	}
	else{
		$('#cur_location').html('Could Not Get Forecast');	
		$('#cur_weather').html('');
		$('#cur_icon').html('');
		$('#cur_temp').html('');
		$('#cur_windchill').html('');
		$('#cur_humidity').html('');
		$('#cur_visibility').html('');
		displayMessage('Could Not Get Forcast');
	}
}

//get weather when user enters input from text box
function getWeatherUserInput(text_input){
	//no text in input
	if(text_input == ""){
		displayMessage('No Input Provided - Using Default');
		getWeatherStateCity('CO','Boulder');
		return;
	}
	
	//attemp to grab weather data from weather underground
	var url = "http://api.wunderground.com/api/" + WU_API_KEY + "/conditions/forecast7day/q/" + escape(text_input) + ".json?callback=?";
	$.getJSON(url, function(data){
		//check data for one result
		if(data.hasOwnProperty('current_observation')){
			updateWeatherData(data);
		}
		//check data for multiple results
		else if(data['response'].hasOwnProperty('results')){
			getWeatherStateCity(data['response']['results'][0]['state'],data['response']['results'][0]['city'])
		}
		//no city found, resort to default 
		else{
			//there was an error finding the city -- default to boulder CO
			displayMessage('Could Not Find Location - Using Default');
			getWeatherStateCity('CO','Boulder');
		}
	});
}

//populate current weather frame
function populateCurrentWeather(location, weather, icon_src, temp, windchill, humidity, vis){
	$('#cur_location').html('<td colspan="2" height="50px"><h2>' + location + '</h2></td>');	
	$('#cur_weather').html('<td colspan="2">' +weather +'<td>');
	$('#cur_icon').html('<td colspan="2"><img src = "'+icon_src+'" width="160" height="160" /></td>');
	$('#cur_temp').html('<td>Temp:</td><td>' + temp + 'F</td>');
	$('#cur_windchill').html('<td>Windchill:</td><td>' + windchill + 'F</td>');
	$('#cur_humidity').html('<td>Humidity:</td><td>' + humidity + '</td>');
	$('#cur_visibility').html('<td>Visibility:</td><td>' + vis + ' MI</td>');
}

//populate forcast frame 
function populateForcast(forcast_days,num_days){
	forcast = '<table id="forcasttable">';
	for(i = 0; i < num_days; i++){
		forcast += '<td>';
		forcast += genForcastDayMarkup(forcast_days[i],num_days);
		forcast += '</td>';
	}
	forcast += '</table>';
	$('#forcastframe').html(forcast);
}

//given a day return table markup
function genForcastDayMarkup(day){
	markup =  '<table>';
	markup += '<tr><td>'+day.day+'</td><td>'+day.day_num+'</td></tr>';
	markup += '<tr><td colspan="2"><img src = "'+day.icon_src+'" width="70" height="70"/></td></tr>';
	markup += '<tr ><td colspan="2" height="35px" >' + day.conditions + '</td></tr>';
	markup += '<tr><td>Hi</td><td>Lo</td></tr>';
	markup += '<tr><td>'+day.high+'</td><td>'+day.low+'</td></tr>';
	markup += '</table>';
	return markup;
}

//class to hold each day
function Day(high,low,icon_src,conditions,day_num, day){
	this.high = high;
	this.low = low;
	this.icon_src = icon_src;
	this.conditions = conditions;
	this.day = day;
	this.day_num = day_num;
}

//create a temperature chart using highcharts
function updateTempsChart(days){
	//extract data from days 
	highs = Array();
	lows = Array();
	labels = Array();
	for(i in days){
		if(days[i].high != ""){
			highs.push(parseInt(days[i].high));
		}
		if(days[i].low != ""){
			lows.push(parseInt(days[i].low));
		}
		labels.push(parseInt(days[i].day_num));
	}
	//create a chart based on the data we collected
	var chart;
	$(document).ready(function() {
		chart = new Highcharts.Chart({
		chart: {
			renderTo: 'chart_container',
			defaultSeriesType: 'line',
			marginRight: 70,
			marginBottom: 25
		},
		title: {
			text: 'Daily Temperature',
			x: -20 //center
		},
		xAxis: {
			categories: labels
		},
		yAxis: {
			title: {
				text: 'Temperature (F)'
			},
			plotLines: [{
				value: 0,
				width: 1,
				color: '#808080'
			}],
			min: Math.min.apply(null, lows),
			max: Math.max.apply(null, highs)
		},
		tooltip: {
			formatter: function() {
				return '<b>'+ this.series.name +'</b><br/>'+
				this.x +': '+ this.y +' F';
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -10,
			y: 100,
			borderWidth: 0
		},
		series: [{
			name: 'Hi',
			color: '#CD5C5C',
			data: highs
		}, {
			name: 'Lo',
			color: '#87CEEB',
			data: lows
		}]
		});
	});
}
 
function displayMessage(text){
	$('.messageBox').html(text);
	$('.messageBox').fadeIn('fast');
	setTimeout(function() {
			$('.messageBox').fadeOut('fast');
	}, 2000); // <-- time in milliseconds
}

